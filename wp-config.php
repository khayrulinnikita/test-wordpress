<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', '!123456789Qq' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J2NmQ7w0^KkH>.~Q_Pp}oJn(Vey>x:sN@);TWy-jj0Pk|Sdv4+u9rxg-oY/eT2it' );
define( 'SECURE_AUTH_KEY',  'J==*cgKCK_~LVzZE>HQj oC4)?{=HfZc#v+9thCH.KiNU}}AR2W2K[!a*fXxXz#|' );
define( 'LOGGED_IN_KEY',    '0Q]PWIb,59;u[R6tN3C|jDO<6VEi04V/>q ^M3z/&n@M{u{X|m<,:[uWc;P!wQX4' );
define( 'NONCE_KEY',        'P)7NN935B.68lNbX,cF>UPF~l4:[%wZ=064L2z*h~g@XyTfRO-K32<X#*?<H2z>Y' );
define( 'AUTH_SALT',        'xvmXe{7qyf=? ,UW]a5rREOjFq05^;Yn+sbY]SWI`iNeLe)AT2epslps|MXypm4N' );
define( 'SECURE_AUTH_SALT', 'mY=+ChTUS-c6KeRmvT5Q&Z6mQSKDb+zT:*$0ac@m%V99+~~Xr#dCS=0ec==7ZXvS' );
define( 'LOGGED_IN_SALT',   'N$1F6~xQX]5i#VH]J|tj sI&k9S~=)_.w>YZn ;Z?Se+Um!mn;zgO{w43&t0kyvb' );
define( 'NONCE_SALT',       'L8b2r;F6uV94O;cQM*w{AJ0/n^gx_T-vMZt|-t L.4U.qleGy{mar@|}|;[BM=LF' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

